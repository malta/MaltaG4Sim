#!/bin/bash

echo "Setting up Geant4"
source /sw/atlas/Geant4/10.05-eb970/x86_64-centos7-gcc8-opt/bin/geant4.sh

export LD_LIBRARY_PATH=/sw/atlas/VecGeom/v1.1.0-22e48/x86_64-centos7-gcc8-opt/lib:$LD_LIBRARY_PATH

