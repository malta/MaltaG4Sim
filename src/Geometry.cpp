#include "Geometry.h"
#include "Detector.h"

#include "G4Material.hh"
#include "G4MaterialTable.hh"
#include "G4Element.hh"
#include "G4ElementTable.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4PVParameterised.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4FieldManager.hh"
#include "G4TransportationManager.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4GDMLParser.hh"
#include "G4SystemOfUnits.hh"
#include "G4RunManager.hh"
#include "G4UniformElectricField.hh"
#include "G4EqMagElectricField.hh"
#include "G4ClassicalRK4.hh"
#include "G4MagIntegratorDriver.hh"
#include "G4ChordFinder.hh"
#include "G4NistManager.hh"
#include <sstream>
#include <vector>
#include "g4root.hh"

Geometry::Geometry()
{
  G4cout << "Geometry::Geometry" << G4endl;

  m_ele["Si"] = new G4Element("Silicon","Si", 14.,  28.09*g/mole);
  m_ele["O"]  = new G4Element("Oxygen", "O",   8.,  16.00*g/mole);
  
  m_mat["Galactic"] = new G4Material("Galactic", 1., 1.01*g/mole, CLHEP::universe_mean_density, kStateGas,0.1*kelvin,1.e-19*pascal);
  m_mat["SiO2"] = new G4Material("Silicon oxide", 2.65*g/cm3, 2);
  m_mat["SiO2"] ->AddElement(m_ele["Si"], 1);
  m_mat["SiO2"] ->AddElement(m_ele["O"],  2);
  
  m_world_length=10.*CLHEP::cm;
  m_world_diameter=3.*CLHEP::cm;
  m_target_diameter=1.*CLHEP::cm;
  m_target_distance=1.*CLHEP::cm;
  m_target_thickness=300.*CLHEP::micrometer;
}

Geometry::~Geometry()
{
  G4cout << "Geometry::~Geometry" << G4endl;
  for(std::map<std::string,G4Material*>::iterator it=m_mat.begin(); it!=m_mat.end(); ++it){
    delete it->second;
  }
  m_mat.clear();
  for(std::map<std::string,G4Element*>::iterator it=m_ele.begin(); it!=m_ele.end(); ++it){
    delete it->second;
  }
  m_ele.clear();  
}


void Geometry::SetTargetDistance(G4float distance){
  m_target_distance=distance;
}

void Geometry::SetTargetThickness(G4float thickness){
  m_target_thickness=thickness;
}

G4VPhysicalVolume* Geometry::Construct()
{
  G4cout << "Geometry::Construct" << G4endl;

  //Store config into output file

  G4AnalysisManager* man = G4AnalysisManager::Instance();
  G4int id=man->CreateNtuple("Metadata", "Metadata");
  man->CreateNtupleDColumn("distance");
  man->CreateNtupleDColumn("thickness");
  man->FinishNtuple();

  man->FillNtupleDColumn(id,0,m_target_distance/CLHEP::cm); 
  man->FillNtupleDColumn(id,1,m_target_thickness/CLHEP::micrometer); 
  man->AddNtupleRow();
 

  //World
  m_world_length=3.*CLHEP::cm;
  m_world_diameter=3.*CLHEP::cm;
  G4Tubs * world_box = new G4Tubs("world_box", 0, m_world_diameter, m_world_length/2.,0.,360.*deg);
  G4LogicalVolume * world_vol = new G4LogicalVolume(world_box,m_mat["Galactic"],"world_vol",0,0,0);  
  G4VPhysicalVolume * world_phy = new G4PVPlacement(0,G4ThreeVector(),world_vol,"world_phy",0,false,0);
  world_vol->SetVisAttributes(G4VisAttributes::GetInvisible());
    
  //Target
  G4Tubs* target_box = new G4Tubs("target_box",0,m_target_diameter,m_target_thickness/2.,0.,360*deg);
  G4LogicalVolume* target_vol = new G4LogicalVolume(target_box,m_mat["SiO2"],"target_vol");
  new G4PVPlacement(0,
                    G4ThreeVector(0,0,m_target_distance+m_target_thickness/2.),
                    target_vol,
                    "target_plv",
                    world_vol,
                    false,
                    0,
                    true);
  
  target_vol->SetVisAttributes(G4Colour::Blue());
  m_sensing.push_back(new Detector("/det/sensor"));
  m_active.push_back(target_vol);
  
  //Return the world physical volume
  return world_phy;
}

void Geometry::ConstructSDandField(){
  G4cout << "Geometry::ConstructSDandField" << G4endl;
  for(unsigned int i=0; i<m_sensing.size(); i++){
    G4SDManager::GetSDMpointer()->AddNewDetector(m_sensing.at(i));
    SetSensitiveDetector(m_active.at(i),m_sensing.at(i));
  }  
} 

