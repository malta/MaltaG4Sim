/************************************************
 * Beamline
 * Geant4 event generator
 * Carlos.Solans@cern.ch
 * November 2019
 ***********************************************/
#include "Beamline.h"

#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"

Beamline::Beamline(float energy, float nparticles, float size){
  m_size=size;
  m_gun = new G4ParticleGun(nparticles); 
  m_gun->SetParticleDefinition(G4ParticleTable::GetParticleTable()->FindParticle("e-"));
  m_gun->SetParticlePosition(G4ThreeVector(0.*cm,0.*cm,0.*cm));  
  m_gun->SetParticleMomentumDirection(G4ThreeVector(0.,0.,1));
  m_gun->SetParticleEnergy(energy);
}

Beamline::~Beamline(){
  delete m_gun;
}

void Beamline::GeneratePrimaries(G4Event* event){
  m_gun->SetParticlePosition(G4ThreeVector(0.*cm,0.*cm,0.*cm));
  m_gun->GeneratePrimaryVertex(event);
}


