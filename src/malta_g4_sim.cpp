/*************************************************
 * G4 simulation
 * Carlos.Solans@cern.ch
 *************************************************/

#include "globals.hh"
#include "G4Timer.hh"
#include "G4RunManager.hh"
#include "G4HepRepFile.hh"
#include "G4VisManager.hh"
#include "G4UImanager.hh"
#include "G4UIExecutive.hh"
#include "G4VisExecutive.hh"

#include "Geometry.h"
#include "Beamline.h"
#include "PhysicsList.h"
#include "SteppingAction.h"

#include "G4ios.hh"
#include "g4root.hh"

#include <cmdl/cmdargs.h>
#include <signal.h>

using namespace std;

void handler(int){
  cout << "You pressed ctrl+c to quit" << endl;
  G4RunManager* runManager = G4RunManager::GetRunManager();
  runManager->AbortRun(true);
}


int main(int argc, char** argv){
  
  //Parse arguments
  CmdArgInt nevents ('n', "nevents", "events", "number of events (1)");
  CmdArgStr ofile('o', "ofile", "file", "output file (output.root)");
  CmdArgInt verbose ('v', "verbose", "level", "verbose level (0)");
  CmdArgFloat distance('s', "distance", "distance", "distance from source in cm");
  CmdArgFloat thickness('t', "thickness", "um", "target thickness in micrometer");
  CmdArgFloat particles('p', "particles", "number", "number of particles per spill (event)");
  CmdArgFloat energy('e', "energy", "GeV", "beam energy in GeV");
  CmdArgBool draw('D', "draw", "output HepRep files");
  
  CmdLine cmd(*argv,
              & nevents,
              & ofile,
              & verbose,
              & distance,
              & thickness,
              & energy,
	      & draw,
              NULL);

  nevents = 10;
  verbose = false;
  ofile = "output.root";
  //distance = 1.*CLHEP::cm;
  //thickness = 300.*CLHEP::micrometer;
  energy = 3.*CLHEP::GeV;
  particles = 1;
  
  CmdArgvIter argv_iter(--argc, ++argv);
  cmd.parse(argv_iter);
  
  //Random seeds
  G4Random::setTheEngine(new CLHEP::RanecuEngine);
  long seeds[2];
  time_t systime = time(NULL);
  seeds[0] = (long) systime;
  seeds[1] = (long) (systime*G4UniformRand());
  G4Random::setTheSeeds(seeds);  

  //Timing 
  G4Timer * timer = new G4Timer();
  timer->Start();

  //Create the run manager
  G4RunManager* runManager = new G4RunManager();
  runManager->SetVerboseLevel(verbose);

  //Create the analysis manager
  G4AnalysisManager* ana = G4AnalysisManager::Instance();
  ana->OpenFile(G4String(ofile));
  
  // Construct a detector 
  Geometry * geometry = new Geometry();

  if(distance.flags()&CmdArg::GIVEN) {geometry->SetTargetDistance(distance*CLHEP::cm);}
  if(thickness.flags()&CmdArg::GIVEN){geometry->SetTargetThickness(thickness*CLHEP::micrometer);}
  
  runManager->SetUserInitialization(geometry);
  
  // Intialize the physics lists
  runManager->SetUserInitialization(new PhysicsList(verbose));

  // Initialize the run manager
  runManager->Initialize();

  // Beam line
  Beamline * beamline = new Beamline(energy*CLHEP::GeV,particles,0);
  runManager->SetUserAction(beamline);

  //Define action to kill bouncing electrons 
  //runManager->SetUserAction(new SteppingAction());
  
  //Produce HepRep files if needed
  if(draw){
    G4UIExecutive * ui = new G4UIExecutive(argc, argv);

	G4VisManager* visManager = new G4VisExecutive();
	visManager->Initialize();
	
	G4UImanager* uiManager = G4UImanager::GetUIpointer();
	//heprep
	uiManager->ApplyCommand("/vis/open G4HepRepFile");
	//uiManager->ApplyCommand("/vis/open RayTracerX");
	//uiManager->ApplyCommand("/vis/open OpenInventor");
	uiManager->ApplyCommand("/vis/viewer/create");
	//  uiManager->ApplyCommand("/vis/sceneHandler/create G4HepRepFile");
	uiManager->ApplyCommand("/vis/scene/create");
	uiManager->ApplyCommand("/vis/drawVolume");
	uiManager->ApplyCommand("/vis/scene/add/axes");
	//uiManager->ApplyCommand("/tracking/verbose 1");
	uiManager->ApplyCommand("/vis/scene/add/trajectories");
	//uiManager->ApplyCommand("/vis/modeling/trajectories/create/drawByCharge");
	//uiManager->ApplyCommand("/vis/modeling/trajectories/drawByCharge-0/default/setDrawStepPts true");
	//uiManager->ApplyCommand("/vis/modeling/trajectories/drawByCharge-0/default/setStepPtsSize 2");
	uiManager->ApplyCommand("/vis/scene/add/hits");
	uiManager->ApplyCommand("/vis/viewer/flush");
	G4cout << "uiManager where are you?" << G4endl;
    
    //geometry->SetUIactive();
    ui->SessionStart();
    delete ui;
  
    //delete visualization manager
    delete visManager;
  
  }else{

    //Run the events
    G4cout << "BeamON" << G4endl;
    signal(SIGINT, handler);  
    signal(SIGTERM, handler);  
    signal(SIGKILL, handler);  
    runManager->BeamOn(nevents);
    G4cout << "BeamOFF" << G4endl;
    
  }


  ana->Write();
  ana->CloseFile();  
  delete ana;

  
  //Delete run manager and nested objects
  delete runManager;

  timer->Stop();
  G4cout << "Simulation time: " << timer->GetRealElapsed() << " s " << G4endl;
  G4cout << "Have a nice day" << G4endl;

  delete timer;
  return 0;
}
