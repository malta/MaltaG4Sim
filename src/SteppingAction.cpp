/****************************
 * Stepping Action
 *
 * Carlos.Solans@cern.ch
 * May 2017
 ****************************/

#include "SteppingAction.h"
#include "G4TrackStatus.hh"
#include "G4RunManager.hh"
#include "G4VAnalysisManager.hh"
#include "G4SystemOfUnits.hh"
#include "G4ios.hh"
#include "g4root.hh"

SteppingAction::SteppingAction(){
  G4cout << "SteppingAction::SteppingAction" << G4endl;
  
  //Create an ntuple
  G4AnalysisManager* man = G4AnalysisManager::Instance();
  G4int id=man->CreateNtuple("Signals", "Signals");
  m_ntupleId = id;
  G4cout << "SteppingAction::SteppingAction " 
         << "Signal ntuple id: " << id << G4endl;
  man->CreateNtupleIColumn(id,"event");
  man->CreateNtupleIColumn(id,"pid");
  man->CreateNtupleDColumn(id,"x");
  man->CreateNtupleDColumn(id,"y");
  man->CreateNtupleDColumn(id,"z");
  man->CreateNtupleDColumn(id,"eDep");
  man->CreateNtupleDColumn(id,"eKin");
  man->FinishNtuple();

  m_detVol = "det_plv";
}

SteppingAction::~SteppingAction(){
  G4cout << "SteppingAction::~SteppingAction" << G4endl;
}

void SteppingAction::UserSteppingAction(const G4Step * aStep){

  if(G4RunManager::GetRunManager()->GetVerboseLevel()>0){
    G4cout << "SteppingAction::UserSteppingAction" << G4endl;
  }
  try{

    if(!aStep->GetTrack()->GetVolume()) return;
    if(!aStep->GetTrack()->GetNextVolume()) return;
    
    G4String vol1 = aStep->GetTrack()->GetVolume()->GetName();
    G4String vol2 = aStep->GetTrack()->GetNextVolume()->GetName();
    
    if(G4RunManager::GetRunManager()->GetVerboseLevel()>0){
      G4cout << "SteppingAction::UserSteppingAction Vol1:" << vol1 << ", Vol2:" << vol2 << G4endl;
    }

    //Store particles crossing into the detector volume
    if(vol1!=m_detVol && vol2==m_detVol){
      
      G4cout << "SteppingAction::UserSteppingAction Store particle" << G4endl;
      G4ThreeVector wpos = aStep->GetPreStepPoint()->GetPosition();
      G4ThreeVector rpos = (aStep->GetPostStepPoint()->GetTouchableHandle()->GetHistory()
                            ->GetTopTransform().Inverse().TransformPoint(G4ThreeVector())); 
      G4ThreeVector lpos = rpos+wpos;
      
      G4int    evt  = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();
      G4int    part = aStep->GetTrack()->GetDefinition()->GetPDGEncoding();
      G4double posx = int(lpos.x()/nanometer)*nanometer/CLHEP::cm;
      G4double posy = int(lpos.y()/nanometer)*nanometer/CLHEP::cm;
      G4double posz = int(lpos.z()/nanometer)*nanometer/CLHEP::cm;
      G4double edep = aStep->GetTotalEnergyDeposit()/CLHEP::keV;
      G4double ekin = aStep->GetTrack()->GetKineticEnergy()/CLHEP::keV;
      
      G4AnalysisManager* man = G4AnalysisManager::Instance();
      man->FillNtupleIColumn(m_ntupleId,0,evt); 
      man->FillNtupleIColumn(m_ntupleId,1,part); 
      man->FillNtupleDColumn(m_ntupleId,2,posx); 
      man->FillNtupleDColumn(m_ntupleId,3,posy); 
      man->FillNtupleDColumn(m_ntupleId,4,posz);
      man->FillNtupleDColumn(m_ntupleId,5,edep);
      man->FillNtupleDColumn(m_ntupleId,6,ekin);
      man->AddNtupleRow(m_ntupleId);
      
    }
    
  }catch(std::exception &e){
    G4cout << "SteppingAction::UserSteppingAction " << e.what();
  }
  if(G4RunManager::GetRunManager()->GetVerboseLevel()>0){
    G4cout << "SteppingAction::UserSteppingAction END" << G4endl;
  }

}
