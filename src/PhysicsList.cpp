/***************************************
 * EM Physics List for X-ray simulation
 *
 * https://twiki.cern.ch/twiki/bin/view/Geant4/LowePenelope
 *
 * Carlos.Solans@cern.ch
 * April 2017
 ***************************************/

#include "PhysicsList.h"

#include "G4Proton.hh"
#include "G4Gamma.hh"
#include "G4Electron.hh"
#include "G4Positron.hh"
#include "G4ProcessManager.hh"
#include "G4eIonisation.hh"
#include "G4eMultipleScattering.hh"
#include "G4eBremsstrahlung.hh"
#include "G4eplusAnnihilation.hh"
#include "G4PhotoElectricEffect.hh"
#include "G4GammaConversion.hh"
#include "G4ComptonScattering.hh"
#include "G4RayleighScattering.hh"
#include "G4VAtomDeexcitation.hh"
#include "G4UAtomicDeexcitation.hh"

#include "G4GenericIon.hh"
#include "G4LossTableManager.hh"
#include "G4EmProcessOptions.hh"

#include "G4PenelopeAnnihilationModel.hh"
#include "G4PenelopeBremsstrahlungModel.hh"
#include "G4PenelopeComptonModel.hh"
#include "G4PenelopeGammaConversionModel.hh"
#include "G4PenelopeIonisationModel.hh"
#include "G4PenelopePhotoElectricModel.hh"
#include "G4PenelopeRayleighModel.hh"

PhysicsList::PhysicsList(G4int /*verbose*/):  G4VUserPhysicsList(){
  G4cout << "PhysicsList::PhysicsList" << G4endl;
}

PhysicsList::~PhysicsList(){
  G4cout << "PhysicsList::~PhysicsList" << G4endl;
}

void PhysicsList::ConstructParticle(){
  G4cout << "PhysicsList::ConstructParticle" << G4endl;
  //bosons
  G4Gamma::GammaDefinition();
  //leptons
  G4Electron::ElectronDefinition();
  G4Positron::PositronDefinition();
  //baryons
  G4Proton::ProtonDefinition();
  G4GenericIon::GenericIonDefinition();
}

void PhysicsList::ConstructProcess(){
  G4cout << "PhysicsList::ConstructProcess" << G4endl;
  //Add transportation first. Not sure what it does...
  AddTransportation();
  
  //construct process per particle
  auto particleIterator=GetParticleIterator();
  //auto particleIterator=theParticleIterator;
  particleIterator->reset();
  
  while( (*particleIterator)() )
  {
    G4ParticleDefinition* particle = particleIterator->value();
    G4ProcessManager* pManager = particle->GetProcessManager();
    G4String particleName = particle->GetParticleName();
    
    //Register processes per particle
    if (particleName == "e-"){
      G4eMultipleScattering* eMulti = new G4eMultipleScattering();
      //eMulti->AddEmModel(1, new G4UrbanMscModel());
      pManager->AddProcess(eMulti,-1, 1,1);
      
      G4eIonisation * eIon = new G4eIonisation();
      G4PenelopeIonisationModel * peIon = new G4PenelopeIonisationModel();
      eIon->SetEmModel(peIon);
      pManager->AddProcess(eIon,-1, 2, 2); 
      
      G4eBremsstrahlung * eBrems = new G4eBremsstrahlung();
      eBrems->SetEmModel(new G4PenelopeBremsstrahlungModel());
      pManager->AddProcess(eBrems,-1,-3, 3); 
    }
    else if (particleName == "e+") {
      G4eplusAnnihilation * eAnni = new G4eplusAnnihilation();
      eAnni->SetEmModel(new G4PenelopeAnnihilationModel());
      pManager->AddProcess(eAnni, 0,-1, 4);
	  
    }else if (particleName == "gamma"){	  
      G4PhotoElectricEffect * gPhot = new G4PhotoElectricEffect();
      gPhot->SetEmModel(new G4PenelopeGammaConversionModel());
      pManager->AddDiscreteProcess(gPhot);
      
      G4ComptonScattering * gComp = new G4ComptonScattering();
      gComp->SetEmModel(new G4PenelopeComptonModel());
      pManager->AddDiscreteProcess(gComp);
      
      G4GammaConversion * gConv = new G4GammaConversion();
      gConv->SetEmModel(new G4PenelopeGammaConversionModel());
      pManager->AddDiscreteProcess(gConv);
      
      G4RayleighScattering * gRayl = new G4RayleighScattering();
      gRayl->SetEmModel(new G4PenelopeRayleighModel());
      pManager->AddDiscreteProcess(gRayl);
    }
  }
  
  //Set cuts
  SetCutValue(10.*CLHEP::nm,"gamma");
  SetCutValue(10.*CLHEP::nm,"e+");
  SetCutValue(1.*CLHEP::nm,"e-");
  SetParticleCuts(10.*CLHEP::nm,G4Gamma::Gamma());
  SetParticleCuts(10.*CLHEP::nm,G4Positron::Positron());
  SetParticleCuts(1.*CLHEP::nm,G4Electron::Electron());
  
  //Em options
  G4EmProcessOptions opt;
  opt.SetFluo(true);
  opt.SetAuger(true);
  opt.SetPIXE(true);

  // Multiple Coulomb scattering
  opt.SetPolarAngleLimit(CLHEP::pi);
  
  // Physics tables
  opt.SetMinEnergy(100*CLHEP::eV);
  opt.SetMaxEnergy(10*CLHEP::TeV);
  opt.SetDEDXBinning(220);
  opt.SetLambdaBinning(220);
  
  // Nuclear stopping
  //pnuc->SetMaxKinEnergy(CLHEP::MeV);
  
  // Deexcitation
  G4VAtomDeexcitation* de = new G4UAtomicDeexcitation();
  G4LossTableManager::Instance()->SetAtomDeexcitation(de);
  de->SetFluo(true);

  DumpCutValuesTable();
}

