/************************************************
 * SensitiveDetector
 * Geant4 sensitive detector
 * Carlos.Solans@cern.ch
 * April 2017
 *
 * This is Geant4 software
 ***********************************************/
#include "Detector.h"

#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4ParticleDefinition.hh"
#include "G4VTouchable.hh"
#include "G4TouchableHistory.hh"
#include "G4ios.hh"
#include "G4SDManager.hh"
#include "G4RunManager.hh"
#include "G4VAnalysisManager.hh"
#include "G4SystemOfUnits.hh"
#include "g4root.hh"
#include <iostream>
#include <exception>

Detector::Detector(G4String name): G4VSensitiveDetector(name){
  
  G4cout << "Detector::Detector" << G4endl;
  G4AnalysisManager* man = G4AnalysisManager::Instance();
  m_hitId=man->CreateNtuple("Hits", "Simple hits");
  G4cout << "Detector::Detector "
         << "Create hit ntuple with id: " << m_hitId << G4endl;
  man->CreateNtupleIColumn(m_hitId,"event");
  man->CreateNtupleIColumn(m_hitId,"pid");
  man->CreateNtupleDColumn(m_hitId,"x");
  man->CreateNtupleDColumn(m_hitId,"y");
  man->CreateNtupleDColumn(m_hitId,"z");
  man->CreateNtupleDColumn(m_hitId,"eDep");
  man->CreateNtupleDColumn(m_hitId,"eKin");
  man->FinishNtuple();
  
  m_evtId=man->CreateNtuple("Sums", "Hits per event");
  G4cout << "Detector::Detector "
         << "Create event ntuple with id: " << m_evtId << G4endl;
  man->CreateNtupleIColumn(m_evtId,"event");
  man->CreateNtupleIColumn(m_evtId,"eDep");
  man->FinishNtuple();

}

Detector::~Detector(){
  //G4cout << "Detector::~Detector" << G4endl;
}

void Detector::Initialize(G4HCofThisEvent* /*hce*/){
  G4int evnum = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();
  G4cout << "Detector::Initialize " << evnum <<G4endl;
  m_edep=0.;
}

G4bool Detector::ProcessHits(G4Step* aStep, G4TouchableHistory* /*aHist*/){

  if(G4RunManager::GetRunManager()->GetVerboseLevel()>0){
    G4cout << "Detector::ProcessHits" << G4endl;
  }

  G4int evnum = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();
  G4String pdef = aStep->GetTrack()->GetDefinition()->GetParticleType();
  //G4cout << "Pdef " << pdef << G4endl;
  
  //G4cout << "Get wpos" << G4endl;
  G4ThreeVector wpos = aStep->GetPreStepPoint()->GetPosition();
  //G4cout << wpos << G4endl;

  //G4cout << "Get rpos" << G4endl;
  G4ThreeVector rpos = (aStep->GetPostStepPoint()->GetTouchableHandle()->GetHistory()
                        ->GetTopTransform().Inverse().TransformPoint(G4ThreeVector())); 
  //G4cout << rpos << G4endl;

  //G4cout << "Get lpos" << G4endl;
  G4ThreeVector lpos = rpos+wpos;
  //G4cout << lpos << G4endl;


  //  G4cout << "Get more" << G4endl;
  G4double edep = aStep->GetTotalEnergyDeposit()/CLHEP::keV;
  G4double posx = int(lpos.x()/nanometer)*nanometer/CLHEP::cm;
  G4double posy = int(lpos.y()/nanometer)*nanometer/CLHEP::cm;
  G4double posz = int(lpos.z()/nanometer)*nanometer/CLHEP::cm;
  G4int    part = aStep->GetTrack()->GetDefinition()->GetPDGEncoding();
  G4double ekin = aStep->GetTrack()->GetKineticEnergy()/CLHEP::keV;
  G4String vol2 = aStep->GetTrack()->GetNextVolume()->GetName();
  G4String vol1 = aStep->GetTrack()->GetVolume()->GetName();

  //Store edep per event
  m_edep += edep; 

  //G4cout << "Get pro1" << G4endl;
  G4String pro1 = ""; 
  if(aStep->GetPreStepPoint()->GetProcessDefinedStep()!=0){
    pro1 = aStep->GetPreStepPoint()->GetProcessDefinedStep()->GetProcessName();
  }
  //G4cout << "Get pro2" << G4endl;
  G4String pro2 = ""; 
  if(aStep->GetPostStepPoint()->GetProcessDefinedStep()!=0){
    pro2 = aStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName();
  }  
  
  
  if(G4RunManager::GetRunManager()->GetVerboseLevel()>2){
    G4cout << " p: " << part 
           << " x: " << posx 
           << " y: " << posy 
           << " z: " << posz  
           << " e: " << edep 
           << " t: " << ekin 
           << " s1: " << pro1
           << " s2: " << pro2
           << " v1: " << vol1
           << " v2: " << vol2
           << G4endl; 
  }
    
  try{
	
    G4AnalysisManager* man = G4AnalysisManager::Instance();
    man->FillNtupleIColumn(m_hitId,0,evnum); 
    man->FillNtupleIColumn(m_hitId,1,part); 
    man->FillNtupleDColumn(m_hitId,2,posx); 
    man->FillNtupleDColumn(m_hitId,3,posy); 
    man->FillNtupleDColumn(m_hitId,4,posz);
    man->FillNtupleDColumn(m_hitId,5,edep);
    man->FillNtupleDColumn(m_hitId,6,ekin);
    man->AddNtupleRow(m_hitId);
    
  }catch(std::exception &e){
    if(G4RunManager::GetRunManager()->GetVerboseLevel()>-1){
      G4cout << "Abort due to exception" << G4endl;
    }
    return false;
  }
  
  if(G4RunManager::GetRunManager()->GetVerboseLevel()>1){
    G4cout << "Detector::ProcessHits" << G4endl;
  }
  
  return true;
}

void Detector::EndOfEvent(G4HCofThisEvent*){
  G4int evnum = G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();
  G4cout << "Detector::EndOfEvent " << evnum << G4endl;  

  G4AnalysisManager* man = G4AnalysisManager::Instance();
  man->FillNtupleIColumn(m_evtId,0,evnum); 
  man->FillNtupleIColumn(m_evtId,1,m_edep); 
  man->AddNtupleRow(m_evtId);

  m_edep=0.;

}
