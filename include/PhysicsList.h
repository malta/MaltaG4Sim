/************************************
 * Physics List
 * Carlos.Solans@cern.ch
 * November 2019
 ************************************/

#ifndef PhysicsList_h
#define PhysicsList_h 1

#include "G4VUserPhysicsList.hh"

class PhysicsList: public G4VUserPhysicsList{

public:
  
  PhysicsList(G4int verbose=1);
  
  virtual ~PhysicsList();
  
  void ConstructParticle();
  
  void ConstructProcess();
    
};
#endif
