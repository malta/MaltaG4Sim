/************************************************
 * Beamline
 * Geant4 event generator
 * Carlos.Solans@cern.ch
 * November 2019
 ***********************************************/
#ifndef Beamline_h
#define Beamline_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"

class G4Event;
class G4ParticleGun;

class Beamline : public G4VUserPrimaryGeneratorAction{
public:
  
  //Constructor
  Beamline(float energy, float nparticles, float size);
  
  //Destructor
  ~Beamline();
  
  //Callback method to get next event
  virtual void GeneratePrimaries(G4Event*);
  
private:

  //Particle gun
  G4ParticleGun* m_gun;  

  //Beamline size
  float m_size;

};

#endif
