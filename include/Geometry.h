/************************************************
 * Detector Geometry
 * Carlos.Solans@cern.ch
 * November 2019
 ***********************************************/
#ifndef Geometry_h
#define Geometry_h 1

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"

#include <map>
#include <vector>
#include <string>

class G4VPhysicalVolume;
class G4Material;
class G4Element;
class G4VSensitiveDetector;
class G4UniformElectricField;
class G4EqMagElectricField;
class G4ClassicalRK4;
class G4MagInt_Driver;
class G4ChordFinder;

class Geometry : public G4VUserDetectorConstruction{

public:
  
  //Constructor
  Geometry();
  
  //Destructor
  ~Geometry();
  
  //Construct the detector
  G4VPhysicalVolume* Construct();

  //Construct non-shared objects
  void ConstructSDandField();

  //Set the target distance
  void SetTargetDistance(G4float distance);

  //Set the target thickness
  void SetTargetThickness(G4float thickness);

private:
  
  float m_world_diameter;
  float m_world_length;
  float m_target_diameter;
  float m_target_distance;
  float m_target_thickness;

  std::map<std::string,G4Material*> m_mat;
  std::map<std::string,G4Element*>  m_ele;
  std::vector<G4LogicalVolume*> m_active;
  std::vector<G4VSensitiveDetector*> m_sensing; 
  
};

#endif

