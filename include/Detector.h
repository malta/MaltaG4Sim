/************************************************
 * Geant4 sensitive detector
 * Carlos.Solans@cern.ch
 * November 2019
 ***********************************************/
#ifndef Detector_h
#define Detector_h 1

#include "G4VSensitiveDetector.hh"

class G4Step;
class G4HCofThisEvent;
class G4TouchableHistory;

class Detector : public G4VSensitiveDetector{

public:
  
  //Constructor
  Detector(G4String name);
  
  //Destructor
  ~Detector();
  
  //Start of the event
  void Initialize(G4HCofThisEvent*);

  //Process every step in the volume of the detector
  G4bool ProcessHits(G4Step*,G4TouchableHistory*);
  
  //End of the event
  void EndOfEvent(G4HCofThisEvent*);
  
private:

  G4int m_hitId;
  G4int m_evtId;
  G4float m_edep;
  
};

#endif
